using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using PhoneExt_Service.Service.Models;
using Microsoft.EntityFrameworkCore;
using Novell.Directory.Ldap;

namespace PhoneExt_Service.Service.Services
{
    public interface IApplicationService 
    {
        Task<BaseResponse> GetAll();
        Task<BaseResponse> GetDataAD();
    }

    public class ApplicationService : IApplicationService
    {
        private readonly PhoneExtContext _context;
        public ApplicationService(PhoneExtContext context) 
        {
            _context = context;
        }
        public async Task<BaseResponse> GetAll() 
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var model = await _context.MasterDataAD.Where(e => e.IsActive).Select(emp => new 
                                { emp.EmployeeName,
                                emp.Email,
                                emp.RegionOffice,
                                emp.Jabatan,
                                emp.Divisi,
                                emp.ExtensionNumber
                            }).ToListAsync();
                var log = await _context.LogDailyJob.Where(e => e.Job_Status == "Success").OrderByDescending(x => x.Id).FirstOrDefaultAsync();

                response.Code = (int)HttpStatusCode.OK;
                response.Status = HttpStatusCode.OK.ToString();
                response.Message = "Get All Data Successfully.";
                response.LastUpdatedData = log.LastUpdated.ToString("dddd, dd MMM yyyy HH.mm");
                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<BaseResponse> GetDataAD() 
        {
            BaseResponse response = new BaseResponse();
            var insertedEmployee = new List<MasterDataAD>();
            Int32 insertedCount = 0;

            try
            {
                // Configuration
                int ldapPort = LdapConnection.DefaultPort;
                string ldaphHost = "ad.first-resources.com";
                string loginDn = "FIRST-RESOURCES\\aldo.serena";
                string password = "Fr123456";

                // TRUNCATE TABLE DATABASE
                var sql = $"TRUNCATE TABLE public.\"MasterDataAD\"";
                await _context.Database.ExecuteSqlRawAsync(sql);

                // Ldap can't read up to 1000+ data from AD
                // must filter per character
                for (char letter = 'A'; letter <= 'Z'; letter++)
                {
                    // Connect to Active Directory
                    using (var con = new LdapConnection())
                    {
                        con.Connect(ldaphHost, ldapPort);
                        con.Bind(loginDn, password);

                        LdapSearchConstraints cons = new LdapSearchConstraints 
                        {
                            MaxResults = 0,
                            ReferralFollowing = true
                        };
                        
                        var queue = con.Search(
                                    "OU=First Resources Group Users,DC=ad,DC=first-resources,DC=com",
                                    LdapConnection.ScopeSub,
                                    $"(&(objectClass=user)(cn={letter}*)(telephoneNumber=*))",
                                    null,
                                    false,
                                    cons
                                    );

                        // LdapMessage message;

                        while (queue.HasMore())
                        {
                            var result = queue.Next();
                            var listAttr = result.GetAttributeSet().Keys.ToList();
                            if (!listAttr.Contains("postOfficeBox"))
                            {
                                var userAD = new MasterDataAD() 
                                {
                                    EmployeeName = result.GetAttribute("cn")?.StringValue,
                                    ExtensionNumber = result.GetAttribute("telephoneNumber")?.StringValue,
                                    UserName = result.GetAttribute("sAMAccountName")?.StringValue,
                                    Email = listAttr.Contains("mail") ? result.GetAttribute("mail")?.StringValue : null,
                                    RegionOffice = listAttr.Contains("physicalDeliveryOfficeName") ? result.GetAttribute("physicalDeliveryOfficeName")?.StringValue : null,
                                    Divisi = listAttr.Contains("department") ? result.GetAttribute("department")?.StringValue : null,
                                    Jabatan = listAttr.Contains("title") ? result.GetAttribute("title")?.StringValue : null,
                                    IsActive = true
                                };
                                userAD.CreatedBy = "System";
                                userAD.CreatedDate = DateTime.Now;
                                userAD.ModifyBy = "System";
                                userAD.ModifyDate = DateTime.Now;
                                await _context.MasterDataAD.AddAsync(userAD);
                                insertedEmployee.Add(userAD);
                            }
                        }
                        con.Disconnect();
                        con.Dispose();
                    }
                }
                insertedCount = insertedEmployee.Count;

                var employeeLog = new LogDailyJob() {
                    InsertedData = insertedCount,
                    Job_Status = "Success",
                    UpdateBy = "System",
                    LastUpdated = DateTime.Now
                };

                await _context.LogDailyJob.AddAsync(employeeLog);
                await _context.SaveChangesAsync();

                response.Code = (int)HttpStatusCode.OK;
                response.Status = HttpStatusCode.OK.ToString();
                response.Message = "Successfully restore data";
            }
            catch (Exception ex)
            {
                var errorLog = new LogDailyJob() {
                        InsertedData = 0,
                        Job_Status = "Failed : " + ex.Message,
                        UpdateBy = "System",
                        LastUpdated = DateTime.Now
                    };
                await _context.LogDailyJob.AddAsync(errorLog);
                await _context.SaveChangesAsync();

                response.Message = ex.Message;
            }
            return response;
        }
    }
}