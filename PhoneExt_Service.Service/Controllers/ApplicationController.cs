using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhoneExt_Service.Service.Services;
using PhoneExt_Service.Service.Models;

namespace PhoneExt_Service.Service.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ApplicationController : ControllerBase
    {
        private IApplicationService _service;

        public ApplicationController(IApplicationService service) 
        {
            _service = service;
        }

        [HttpGet("GetAll")]
        public async Task<BaseResponse> GetAll() 
        {
            return await _service.GetAll();
        }

        [HttpGet("GetDataDailyJob")]
        public async Task<BaseResponse> GetDataDailyJob() 
        {
            return await _service.GetDataAD();
        }
    }
}