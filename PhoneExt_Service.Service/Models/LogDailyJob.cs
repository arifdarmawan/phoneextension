using System;

namespace PhoneExt_Service.Service.Models
{
    public class LogDailyJob
    {
        public int Id { get; set; }
        public int InsertedData { get; set; }
        public string Job_Status { get; set; }
        public string UpdateBy { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}