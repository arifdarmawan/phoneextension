using System;
using System.ComponentModel.DataAnnotations;

namespace PhoneExt_Service.Service.Models
{
    public class MasterDataAD
    {
        public string EmployeeName { get; set; }
        [Key]
        public string UserName { get; set; }
        public string Email { get; set; }
        public string RegionOffice { get; set; }
        public string Jabatan { get; set; }
        public string Divisi { get; set; }
        public string ExtensionNumber { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}