using Microsoft.EntityFrameworkCore;

namespace PhoneExt_Service.Service.Models
{
    public class PhoneExtContext : DbContext
    {
        public PhoneExtContext(DbContextOptions<PhoneExtContext> options)
            : base(options)
        {
        }
        public DbSet<MasterDataAD> MasterDataAD { get; set; }
        public DbSet<LogDailyJob> LogDailyJob { get; set; }
        
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //     => optionsBuilder.UseNpgsql("Host=localhost;Database=PhoneExtDatabase;Username=usertest;Password=123456");

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<MasterDataAD>(entity => 
            {
                entity.HasKey(e => e.UserName)
                    .HasName("UserName_pkey");
            });

            base.OnModelCreating(builder);
        }

    }
}