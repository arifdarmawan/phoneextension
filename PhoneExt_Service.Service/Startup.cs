using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Reflection;
using System.IO;
using PhoneExt_Service.Service.Services;
using PhoneExt_Service.Service.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpOverrides;
using System.Net;

namespace PhoneExt_Service.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        readonly string PhoneExtensionAllowCORS = "_phoneExtensionAllowCORS";
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            string[] domains = new string[] {
                Configuration.GetValue<string>("Settings:PhoneExtensionDomain")
            };

            services.AddCors(options =>
            {
                options.AddPolicy(PhoneExtensionAllowCORS, builder =>
                {
                    builder.WithOrigins(domains);
                });
            });

            services.AddScoped<IApplicationService, ApplicationService>();

            // Connect to database
            // services.AddDbContext<PhoneExtContext>(opt => opt.UseNpgsql(Configuration.GetConnectionString("Settings:ConnectionString")));
            var connection = Configuration.GetValue<string>("Settings:ConnectionString");

            if (!string.IsNullOrWhiteSpace(connection))
            {
                services.AddDbContext<PhoneExtContext>(opts =>
                {
                    opts.UseNpgsql(connection);
                });
            }

            services.AddSwaggerGen();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v2", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Phone Ext App Services",
                    Version = "v2",
                    Description = "Application Services Library | Phone Ext App | @first-resources.com",
                });
            });

            // Adds a trusted proxy server at specific IP address
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.KnownProxies.Add(IPAddress.Parse("10.100.1.24"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Configure a proxy server
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            app.UseAuthentication();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "Phone Ext App Services V2");
                c.RoutePrefix = string.Empty;
                // Hide schemas in Swagger UI
                c.DefaultModelsExpandDepth(-1);
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
