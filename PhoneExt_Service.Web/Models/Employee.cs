using System;
using System.ComponentModel.DataAnnotations;

namespace PhoneExt_Service.Web.Models
{
    public class Employee
    {
        public string EmployeeName { get; set; }
        public string Email { get; set; }
        public string RegionOffice { get; set; }
        public string Jabatan { get; set; }
        public string Divisi { get; set; }
        public string ExtensionNumber { get; set; }
    }
}