using System;
using System.Net.Http;
using System.Threading.Tasks;
using PhoneExt_Service.Web.Models;

namespace PhoneExt_Service.Web.Services
{
    public class GlobalService
    {
        HttpClient _client { get; }
        public GlobalService(HttpClient client) 
        {
            _client = client;
        }

        public async Task<BaseResponse> GetAsync(string url) 
        {
            var result = new BaseResponse();

            try
            {
                var response = await _client.GetAsync(url);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }
    }
}