﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PhoneExt_Service.Web.Models;
using PhoneExt_Service.Web.Services;

namespace PhoneExt_Service.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly GlobalService _service;
        readonly IConfiguration _configuration;

        public HomeController(ILogger<HomeController> logger, GlobalService service, IConfiguration configuration)
        {
            _logger = logger;
            _service = service;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> LoadData() 
        {
            var result = await _service.GetAsync(_configuration.GetValue<string>("PhoneExtensionAPIEndpoint:GetAll"));
            var userData = result.Data != null ? JsonConvert.DeserializeObject<List<Employee>>(Convert.ToString(result.Data)) : null;
            return Json(new {data = userData, lastUpdate = result.LastUpdatedData});
        }
    }
}
